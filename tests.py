import unittest

from parameterized import parameterized
import pptx
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import PP_ALIGN

import pptx_ataskaita
P = pptx_ataskaita.Picture
NL = pptx_ataskaita.NamedList


class TestFunctions(unittest.TestCase):
    def test_slides_struct(self):
        path = "D:\\Python\\pptx_ataskaita\\Data"
        result = pptx_ataskaita.slides_struct(path)
        path += '\\'
        t = 'Pakartotiniai pažeidimai\\'
        pt = path + t
        expected = [
            NL('Nauji pažeidimai', [
                NL('Dirbtuvės', [
                    path+'Nauji pažeidimai\\Dirbtuvės\\10020hall.jpg']),
                NL('Sandėlys', [
                    path+'Nauji pažeidimai\\Sandėlys\\kelias.jpg',
                    path+'Nauji pažeidimai\\Sandėlys\\Labiau užstrigęs.jpg',
                    path+'Nauji pažeidimai\\Sandėlys\\užstrigęs keltuvas.jpg'])
            ]),
            NL('Pakartotiniai pažeidimai', [
                NL('Dirbtuvės', [
                    pt+'1Dirbtuvės\\d100205697.jpg']),
                NL('Administracija', [
                    pt+'Administracija\\Betvarkis susirinkimas.jpg',
                    pt+'Administracija\\Didžiulis užimtumas.jpg',
                    pt+'Administracija\\Dirbanti asistentė.jpg',
                    pt+'Administracija\\Popieriai pokalbio fone (1).jpg',
                    pt+'Administracija\\Popieriai pokalbio fone (2).jpg']),
                NL('Akumuliatorinė', [
                    pt+'Akumuliatorinė\\950 ak295.jpg']),
            ])
        ]
        self.assertEqual(expected, result)

    def test_section_struct(self):
        path = "D:\\Python\\pptx_ataskaita\\Data\\Nauji pažeidimai"
        result = pptx_ataskaita.section_struct(path)
        path += '\\'
        expected = [
            NL('Dirbtuvės', [
                path+'Dirbtuvės\\10020hall.jpg']),
            NL('Sandėlys', [
                path+'Sandėlys\\kelias.jpg',
                path+'Sandėlys\\Labiau užstrigęs.jpg',
                path+'Sandėlys\\užstrigęs keltuvas.jpg'])
        ]
        self.assertEqual(expected, result)

    @parameterized.expand([
        (100, 4, 0.05, 18.75),
        (48000, 1, 0.1, 38400),
    ])
    def test_max_width(self, width, n, spacing, expected):
        self.assertEqual(
            expected,
            pptx_ataskaita.max_width(n, width, spacing)
        )

    @parameterized.expand([
        (50, 0, 0.2),
        (7580, 14, 0.1),
    ])
    def test_max_width_exceptions(self, width, n, spacing):
        self.assertRaises(
            ValueError,
            pptx_ataskaita.max_width, n, width, spacing
        )

    @parameterized.expand([
        (1, 10, 0.1, [(1, 8)]),
        (2, 10, 0.05, [(0.5, 4.25), (5.25, 4.25)])
    ])
    def test_elem_pos(self, n, w, spacing, expected):
        gen = pptx_ataskaita.elem_pos(n, w, spacing)
        result = [i for i in gen]
        self.assertEqual(expected, result)

    def test_text_pos(self):
        result = pptx_ataskaita.text_pos(10, 5, 20, 0.75, 0.1)
        expected = (10, 15, 5, 2)
        self.assertEqual(expected, result)

    def test_title_pos(self):
        result = pptx_ataskaita.title_pos(
            4, 200, 150, spacing=0.1,
            top_rel=0.15, height_rel=0.2
        )
        expected = (20, 22.5, 160, 30)
        self.assertEqual(expected, result)

    def test_picture_pos(self):
        result = pptx_ataskaita.picture_pos(10, 5, 20, 0.25, 0.4)
        expected = (10, 5, 5, 8)
        self.assertEqual(expected, result)

    @parameterized.expand([
        ("D:\\Python\\pptx_ataskaita\\Data", "Data"),
        ('Administracija\\Betvarkis susirinkimas.jpg',
         'Betvarkis susirinkimas'),
        ('Popieriai pokalbio fone.jpg', 'Popieriai pokalbio fone'),
        ('sausainiai', 'sausainiai')
    ])
    def test_fname(self, path, expected):
        self.assertEqual(expected, pptx_ataskaita.fname(path))


class TestAddShapes(unittest.TestCase):
    def setUp(self):
        self.prs = pptx.Presentation()
        self.layout = self.prs.slide_layouts[0]
        self.slide = self.prs.slides.add_slide(self.layout)

    def test_add_text_box(self):
        pos = (1, 1, 1, 1)
        self.assertIsInstance(
            pptx_ataskaita.add_text_box(self.slide, pos),
            pptx.shapes.autoshape.Shape
        )

    def test_add_picture(self):
        pos = (1, 1)
        path = 'D:\\Python\\pptx_ataskaita\\logo.png'
        self.assertIsInstance(
            pptx_ataskaita.add_picture(self.slide, path, pos),
            pptx.shapes.picture.Picture
        )

    @parameterized.expand([
        ([NL('a', ['b'])], [(P('a', 'b', 'b'),)]),
        ([NL('a', ['a', 'b', 'c', 'd'])], [(
            P('a', 'a', 'a'), P('a', 'b', 'b'),
            P('a', 'c', 'c'), P('a', 'd', 'd')
        )]),
        ([NL('a', ['a', 'b.jpg']), NL('c', ['c.a', 'd', 'e', 'f', 'g'])], [
            (P('a', 'a', 'a'), P('a', 'b.jpg', 'b')),
            (P('c', 'c.a', 'c'), P('c', 'd', 'd'),
                P('c', 'e', 'e'), P('c', 'f', 'f')
             ),
            (P('c', 'g', 'g'),)
        ]),
    ])
    def test_slides_list(self, locations, expected, limit=4):
        self.assertEqual(
            pptx_ataskaita.slides_list(
                locations,
                limit
            ),
            expected
        )


class TestTextManipulation(unittest.TestCase):
    def setUp(self):
        self.prs = pptx.Presentation()
        self.layout = self.prs.slide_layouts[0]
        self.slide = self.prs.slides.add_slide(self.layout)
        pos = (1, 1, 1, 1)
        self.shape = self.slide.shapes.add_shape(
            MSO_SHAPE.ROUNDED_RECTANGLE, *pos)

    def test_assign_text(self):
        text = 'The house was green'
        pptx_ataskaita.assign_text(self.shape, text)
        self.assertEqual(text, self.shape.text_frame.text)

    @parameterized.expand([
        ('left', PP_ALIGN.LEFT),
        ('right', PP_ALIGN.RIGHT),
        ('center', PP_ALIGN.CENTER),
        ('just', PP_ALIGN.JUSTIFY),
    ])
    def test_align_text(self, align, expected):
        pptx_ataskaita.align_text(self.shape, align)
        self.assertEqual(
            expected,
            self.shape.text_frame.paragraphs[0].alignment
        )

    @parameterized.expand([
        ('hello', 'hello'),
        ('1cookies', 'cookies'),
        ('2_my cookies', 'my cookies'),
        ('7 lovely74_high', 'lovely74_high'),
        ('gal45  free', 'gal45  free'),
        ('mouse(1)', 'mouse'),
        ('icecream (786)', 'icecream'),
        ('2_all good should be (12)', 'all good should be')
    ])
    def test_remove_syntax_nums(self, text, expected):
        result = pptx_ataskaita.remove_syntax_nums(text)
        self.assertEqual(result, expected)


class TestPptxFunctions(unittest.TestCase):
    def setUp(self):
        self.prs = pptx.Presentation()
        self.layout = self.prs.slide_layouts[0]

    @parameterized.expand([
        ('Blank', 'Blank'),
        ('Title Slide', 'Title Slide'),
    ])
    def test_get_layout_by_name(self, name, expected):
        layout = pptx_ataskaita.get_layout_by_name(self.prs, name)
        self.assertEqual(expected, layout.name)

    def test_get_layout_by_name_name_not_found(self):
        self.assertRaises(
            ValueError,
            pptx_ataskaita.get_layout_by_name,
            self.prs, 'Title'
        )

    def test_slide_dimensions(self):
        self.assertEqual(
            (9144000, 6858000),
            pptx_ataskaita.slide_dimensions(self.prs)
        )

    def test_add_slide(self):
        self.assertIsInstance(
            pptx_ataskaita.add_slide(self.prs, self.layout),
            pptx.slide.Slide
        )


class TestDataStructures(unittest.TestCase):

    def test_NamedList(self):
        namedlist = pptx_ataskaita.NamedList('Admin')
        self.assertEqual(namedlist.name, 'Admin')
        self.assertTrue(isinstance(namedlist, list))

    @parameterized.expand([
        (('hello', []), ('hello', [1]), False),
        (('a', [4, 5]), ('a', [4, 5]), True),
        (('a', []), ('b', []), False),
        (('a', ['b']), ('b', ['a']), False)
    ])
    def test_NamedList_comparison(self, a, b, expected):
        a = pptx_ataskaita.NamedList(*a)
        b = pptx_ataskaita.NamedList(*b)
        self.assertEqual(a == b, expected)

    def test_Picture(self):
        p = pptx_ataskaita.Picture('a', 'b', 'c')
        self.assertEqual(p.location, 'a')
        self.assertEqual(p.path, 'b')
        self.assertEqual(p.text, 'c')

    @parameterized.expand([
        (('a', 'b', 'c'), ('a', 'b', 'c'), True),
        (('a', 'b', 'c'), ('b', 'c', 'a'), False),
        (('a', 'c', 'b'), ('a', 'b', 'c'), False)
    ])
    def test_Picture_comparison(self, a, b, expected):
        a = pptx_ataskaita.Picture(*a)
        b = pptx_ataskaita.Picture(*b)
        self.assertEqual(a == b, expected)


def runtests():
    unittest.main()


if __name__ == '__main__':
    runtests()
