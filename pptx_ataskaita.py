import os
import sys
import datetime

import pptx
from pptx.enum.shapes import MSO_SHAPE

import PIL

import re


BOX = MSO_SHAPE.ROUNDED_RECTANGLE
ALIGN_TYPES = {
    'left': 1, 'l': 1,
    'center': 2, 'ctr': 2, 'c': 2,
    'right': 3, 'r': 3,
    'justify': 4, 'just': 4, 'j': 4,
    'distribute': 5, 'dist': 5, 'd': 5,
    'thai distribute': 6,
    'justify low': 7,
    }

# Constants
HORIZONTAL_SPACING_REL = 0.05  # In parts of width
VERTICAL_SPACING_REL = 0.05
TITLE_BOX_TOP_REL = 0.05
TITLE_BOX_HEIGHT_REL = 0.15
PICT_TOP_REL = TITLE_BOX_TOP_REL+TITLE_BOX_HEIGHT_REL+VERTICAL_SPACING_REL
PICT_HEIGHT_REL = 0.4
TEXT_BOX_TOP_REL = PICT_TOP_REL+PICT_HEIGHT_REL+VERTICAL_SPACING_REL
TEXT_BOX_HEIGHT_REL = 0.1
MAX_PICTURES_PER_SLIDE = 4

TEMPLATE_NAME = 'šablonas.pptx'

TEXT_DEFAULTS = {
    'font_rgb_color': (199, 0, 0),
    'align': 'center',
    'font': 'Arial',
    'size': 12,  # in Pt
    'bold': 0,
    'italic': 0,
    }
TITLE_DEFAULTS = {
    'replace': [('<data>', datetime.datetime.today().strftime('%Y-%m-%d'))]
}


class NamedList(list):

    def __init__(self, name, ls=None):
        if ls is None:
            ls = []
        super().__init__(ls)
        self.name = name

    def __eq__(self, other):
        return (super().__eq__(other) and
                self.name == getattr(other, 'name', False))


class Picture():

    def __init__(self, location, path, text):
        self.location = location
        self.path = path
        self.text = text

    def __eq__(self, other):
        return (isinstance(other, Picture)
                and self.location == other.location
                and self.path == other.path
                and self.text == other.text
                )

    def __repr__(self):
        return f'Picture({self.location}, {self.path}, {self.text})'

    def __hash__(self):
        return hash(self.location+self.path+self.text)


def slides_struct(path):
    structure = []
    for section in os.listdir(path):
        structure.append(
            NamedList(
                remove_syntax_nums(section),
                section_struct(path+'\\'+section)
            )
        )
    return structure


def section_struct(section_path):
    """
    Takes folder path. Returns a list of [folder name, [pictures paths lists]]
    """
    ext = ['png', 'jpg', 'bmp', 'jpeg']
    folder_paths = [section_path+'\\'+f for f in os.listdir(section_path)]
    locations = []
    for fp in folder_paths:
        paths = [
            fp+'\\'+f
            for f in os.listdir(fp)
            if f.split('.')[-1] in ext
        ]
        if paths:
            locations.append(
                NamedList(
                    remove_syntax_nums(fname(fp)),
                    paths
                )
            )
    return locations


def get_layout_by_name(presentation, name='Blank'):
    """
    Takes presentation object and layout name. Returns layout object with
    matching name.
    """
    for layout in presentation.slide_layouts:
        if layout.name == name:
            return layout
    raise ValueError('Layout name not found')


def build_pptx(folder, reporting=False):

    if reporting:
        print('Analizuojama ataskaitos struktūra...')

    # For script packaged with pyinstaller
    if getattr(sys, 'frozen', False):
        template = os.path.dirname(sys.executable) + '\\' + TEMPLATE_NAME
    else:
        template = os.curdir + '\\' + TEMPLATE_NAME

    if os.path.isfile(template):
        if reporting:
            print(f'Generuojama ataskaita {fname(folder)}')

        prs = pptx.Presentation(template)
        add_dimensions_to_slides(prs)  # Might require repairs

        build_title_slide(prs)
        build_sections(prs, folder)
        build_final_slide(prs)

        try:
            prs.save(f'{fname(folder)}.pptx')
            if reporting:
                print(f'Ataskaita "{fname(folder)}.pptx" išsaugota.')
        except PermissionError:
            print(f'KLAIDA: ataskaitos {fname(folder)}.pptx nepavyko'
                  + ' išsaugoti. Uždarykite bylą {fname(folder)}.pptx')

    else:
        print('NERASTA ŠABLONO BYLA. ŠALIA SKRIPTO PRIVALO BŪTI'
              + '"šablonas.pptx" BYLA.')


def build_sections(prs, folder):
    for section in slides_struct(folder):
        build_section_slide(prs, section.name)
        build_content_slides(prs, section)


def build_title_slide(prs, user_settings={}):
    settings = TITLE_DEFAULTS.copy()
    for key, value in user_settings.items():
        settings[key] = value
    slide = prs.slides[0]
    for shape in slide.shapes:
        for target, replace in settings['replace']:
            if shape.has_text_frame and target in shape.text:
                replace_text(shape, target, replace)


def build_section_slide(prs, name, user_settings={}):
    slide = add_slide(prs, 'Blank')
    width = 0.7 * slide.width
    height = 0.2 * slide.height
    left = (slide.width - width) * 0.5
    top = (slide.height - height) * 0.5
    pos = (left, top, width, height)
    text_box = add_text_box(slide, pos)
    assign_text(text_box, name)
    format_shape_text(text_box, {'size': 24})


def build_content_slides(prs, locations):
    for slide_cont in slides_content(locations):
        slide = add_slide(prs, 'Blank')
        render_titles(slide, title_list(slide_cont))
        render_contents(slide, slide_cont)


def build_final_slide(prs, user_settings={}):
    move_slide(prs, 1, len(prs.slides)-1)


def move_slide(prs, old_index, new_index):
    xml_slides = prs.slides._sldIdLst
    slides = list(xml_slides)
    xml_slides.remove(slides[old_index])
    xml_slides.insert(new_index, slides[old_index])


def render_contents(slide, slide_cont):
    text = ''
    same_text = {}
    for p in slide_cont:
        if p.text == text:
            if text in same_text:
                same_text[text] += 1
            else:
                same_text[text] = 2
        else:
            text = p.text

    counter = 0
    for pic, pos in zip(
        slide_cont,
        elem_pos(len(slide_cont), slide.width)
    ):
        render_picture(slide, pic, *pos)
        if pic.text in same_text:
            if counter == 0:
                multi_pos = pos[:]
            else:
                multi_pos = (multi_pos[0], pos[0] - multi_pos[0] + pos[1])
            counter += 1
            if counter == same_text[pic.text]:
                text_box = render_text(slide, pic, *multi_pos)
                format_shape_text(text_box, {'font_rgb_color': (0, 0, 0)})
        else:
            text_box = render_text(slide, pic, *pos)
            format_shape_text(text_box, {'font_rgb_color': (0, 0, 0)})


def render_picture(slide, data, left, width):
    pict_pos = picture_pos(left, width, slide.height)
    picture = add_picture(slide, data.path, pict_pos)
    return picture


def render_text(slide, data, left, width, text=None):
    text_box = add_text_box(slide, text_pos(left, width, slide.height))
    if text is None:
        text = data.text
    assign_text(text_box, text)
    return text_box


def render_titles(slide, titles_list):
    """
    Takes pptx.slide.Slide object and list of dictionaries in form of
    {'text': title_text, 'n': number of pictures for title}. Adds title
    objects to slide based on pictures count per title.
    """
    total = sum([a['n'] for a in titles_list])
    before = 0
    after = total
    for title in titles_list:
        after -= title['n']
        t_pos = title_pos(
            title['n'], slide.width, slide.height,
            before=before, after=after
        )
        title_box = add_text_box(slide, t_pos)
        align_text(title_box, 'center')
        assign_text(title_box, fname(title['text']))
        format_shape_text(title_box, {'size': 20, 'bold': True})
        before += title['n']


def add_dimensions_to_slides(prs):
    """
    Adds dimensions to pptx.slide.Slide class to be used in later parts.
    Sort of a hack. Don't like it, because ALL instances of Slide has same
    values.
    """
    width, height = slide_dimensions(prs)
    pptx.slide.Slide.width = width
    pptx.slide.Slide.height = height


def max_width(n, width, spacing=HORIZONTAL_SPACING_REL):
    """
    Returns space left for object based on spacing parameter and object count.
    """
    if n == 0:
        raise ValueError('0 is unaccaptable value for n.')
    if spacing * (n+1) >= 1:
        raise ValueError('Too much spacing. No space for pictures.')
    return (width * (1 - spacing * (n+1))) / n


def add_slide(prs, layout='Blank'):
    """
    Adds a new slide to presentation. Layout can be pptx.slide.SlideLayout
    object, name of the layout or layouts id.
    Returns new instance of pptx.slide.Slide object.
    """
    if isinstance(layout, str):
        layout = get_layout_by_name(prs, layout)
    elif isinstance(layout, int):
        layout = prs.slide_layouts[layout]
    return prs.slides.add_slide(layout)


def slide_dimensions(prs):
    """
    Returns (width obj, height obj) for presentation where objects are
    pptx.util.Length class.
    """
    return (prs.slide_width, prs.slide_height)


def elem_pos(n, slide_width, spacing=HORIZONTAL_SPACING_REL):
    width = max_width(n, slide_width, spacing)
    spacing_abs = spacing * slide_width
    i = 0
    while i < n:
        left = spacing_abs + i*(width+spacing_abs)
        yield (left, width)
        i += 1


def title_pos(
        count, slide_width, slide_height, before=0, after=0,
        spacing=HORIZONTAL_SPACING_REL,
        top_rel=TITLE_BOX_TOP_REL,
        height_rel=TITLE_BOX_HEIGHT_REL
        ):
    m_w = max_width(count+before+after, slide_width, spacing)
    spacing_abs = spacing * slide_width
    left = before * (m_w+spacing_abs) + spacing_abs
    top = slide_height * top_rel
    width = (m_w+spacing_abs) * count - spacing_abs
    height = slide_height * height_rel
    return (left, top, width, height)


def text_pos(
        left, width, slide_height,
        top_rel=TEXT_BOX_TOP_REL,
        height_rel=TEXT_BOX_HEIGHT_REL
        ):
    top = slide_height * top_rel
    height = slide_height * height_rel
    return (left, top, width, height)


def picture_pos(
        left, width, slide_height,
        top_rel=PICT_TOP_REL,
        height_rel=PICT_HEIGHT_REL,
        ):
    top = top_rel * slide_height
    height = height_rel * slide_height
    return (left, top, width, height)


def fname(path):
    """
    Takes full path or just file name and returns file name without extension.
    """
    if path[-1:] == '\\':
        path = path[:-1]
    name = os.path.basename(path)
    if '.' in name:
        name = ''.join(name.split('.')[:-1])
    return name


def assign_text(shape, text):
    shape.text_frame.text = text


def replace_text(shape, target, replace=''):
    for p in shape.text_frame.paragraphs:
            for r in p.runs:
                if target in r.text:
                    r.text = r.text.replace(target, replace)


def align_text(shape, align):
    if isinstance(align, str):
        align = ALIGN_TYPES[align]
    if align in [i for i in range(1, 8)]:
        for p in shape.text_frame.paragraphs:
            p.alignment = align


def add_picture(slide, path, pos):
    if len(pos) > 2:
        pos = picture_dimensions(path, *pos)
    return slide.shapes.add_picture(path, *pos)


def picture_dimensions(path, left, top, b_width, b_height):
    image = PIL.Image.open(path)
    i_width, i_height = image.size
    image.close()
    im_ratio = i_width / i_height
    b_ratio = b_width / b_height
    if im_ratio > b_ratio:
        width = b_width
        height = i_height * (b_width / i_width)
        top += (b_height - height) / 2
    else:
        width = i_width * (b_height / i_height)
        height = b_height
        left += (b_width - width) / 2
    return (left, top, width, height)


def add_text_box(slide, pos, set_design=True):
    text_box = slide.shapes.add_shape(BOX, *pos)
    if set_design:
        text_box.fill.background()
        text_box.line.fill.background()
        text_box.text_frame.word_wrap = True
    return text_box


def format_shape_text(shape, user_settings={}):
    settings = TEXT_DEFAULTS.copy()
    for key, value in user_settings.items():
        settings[key] = value

    align_text(shape, settings['align'])
    for p in shape.text_frame.paragraphs:
        p.font.color.rgb = pptx.dml.color.RGBColor(
                                *settings['font_rgb_color'])
        p.font.name = settings['font']
        p.font.size = pptx.util.Pt(settings['size'])
        if settings['bold'] != 0:
            p.font.bold = settings['bold']
        if settings['italic'] != 0:
            p.font.italic = settings['italic']


def slides_list(locations, limit=MAX_PICTURES_PER_SLIDE):
    """
    Takes a list of NamedList.
    Returns Pictures list with inseparable pictures isolated in tuples.
    """
    pictures = []
    for loc in locations:
        if len(loc) == 0:
            continue
        else:
            a = not (len(loc) % limit == 0)
            for n in range(len(loc)//limit+a):
                pictures.append([Picture(
                        loc.name,
                        path,
                        remove_syntax_nums(fname(path))
                        )
                    for path in loc[n*limit:(n+1)*limit]
                ])
    return pictures


def slides_content(locations, limit=MAX_PICTURES_PER_SLIDE):
    """
    Takes a list of locations as NamedLists and return list of tuples, where
    each tuple is slide-worth of Pictures.
    """
    slides = slides_list(locations)
    i = 0
    while i < len(slides):

        pics_in_slide = len(slides[i])
        r = 0
        while pics_in_slide < limit:
            r += 1
            if i + r < len(slides):
                pics_in_slide += len(slides[i+r])
            else:
                break

        if pics_in_slide == limit:
            r += 1

        # join slides [i:i+r]
        if r > 1:
            slides[i] = [pic for slide in slides[i:i+r] for pic in slide]
            slides = slides[:i+1] + slides[i+r:]

        i += 1
    keep_same_text_together(slides)
    return slides


def title_list(slide_cont):
    result = []
    location = ''
    for d in slide_cont:
        if location == d.location:
            result[-1]['n'] += 1
        else:
            result.append({'text': d.location, 'n': 1})
            location = d.location
    return result


def remove_syntax_nums(text):
    ordering_pattern = re.compile('^[0-9]*_* *')
    multi_pic_text_pattern = re.compile('\s*[(]*[0-9]*[)]*$')
    text = re.sub(ordering_pattern, '', text)
    text = re.sub(multi_pic_text_pattern, '', text)
    return text


def keep_same_text_together(slides, limit=MAX_PICTURES_PER_SLIDE):
    """
    If pictures have same text they will be kept on same slide.
    Pictures can be spread out only over two slides to be kept together and
    there has to be no more than can fit in a single slide.
    """
    p = Picture('', '', '')
    sld = None
    groupings = []
    group = []
    for slide in slides:
        for pic in slide:
            if p.text == pic.text:
                if group:
                    group.append([pic, slide])
                else:
                    group.append([p, sld])
                    group.append([pic, slide])
            else:
                if group:
                    groupings.append(group)
                    group = []
                p = pic
                sld = slide

    problematic = []
    for group in groupings:
        if group[0][-1] != group[-1][-1]:
            problematic.append([
                len(group),
                group[0][1].index(group[0][0]),
                [group[0][1], group[-1][1]]
            ])

    unmovable = set(pair[0] for group in groupings for pair in group)
    for group in problematic:
        sld1, sld2 = group[-1]
        count, first = group[0], group[1]
        last = count + first - limit - 1
        if count > limit:
            pass  # too many pictures to fit in one slide

        elif len(sld2) - first <= 0:  # sld2 can fit everything
            for _ in range(first, limit):
                sld2.insert(0, sld1.pop())

        elif (sld1[first-1].location == sld1[last].location
              and sld1[first-1] not in unmovable
              ):
            sld2[last], sld1[first-1] = sld1[first-1], sld2[last]

        elif (sld2[last+1].location == sld2[first].location
              and sld[last+1] not in unmovable
              ):
            sld2[last+1], sld1[first] = sld1[first], sld2[last+1]

        else:  # final resort - aggregate pictures into new slide
            ls = []
            for _ in range(first, limit):
                ls.append(sld1.pop())
            for _ in range(last+1):
                ls.append(sld2.pop(0))
            slides.insert(slides.index(sld1)+1, tuple(ls))


if __name__ == "__main__":
    folders = sys.argv[1:]
    if folders:
        for folder in folders:
            build_pptx(folder, reporting=True)
        input('Darbas baigtas. Paspauskite Enter uždaryti langui...')
    else:
        input('Nepateikti darbiniai duomenys. Norėdami naudoti skriptą'
              + ' uždarę šį langą užtempkite paruoštą aplanką'
              + ' ant skripto. Paspauskite Enter uždaryti langui...')
